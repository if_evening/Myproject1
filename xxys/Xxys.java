/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xxys;
import java.awt.Dimension;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Random;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author LENOVO
 */
public class Xxys extends JFrame{
      public JPanel Panel1;
      public JLabel graselect;//年级选择
      public JLabel questnum;//题目数量
      public ButtonGroup buttonGroup;
      public JTextField textnum;//添加题目数量
      public JButton  chuti;//出题
      public JButton   chongzhi;//重置
      public  JTextArea  quest;//出题的地方
      public JRadioButton gradeone;
      public JRadioButton gradetwo;
      public JRadioButton gradethree;
      public JRadioButton gradefour;
      public JRadioButton gradefive;
      public JRadioButton gradesix;
      
     public static void main(String[] args) {
         try{
            Xxys jm=new Xxys();
              jm.setVisible(true);
             }
        catch(Exception e){
          e.printStackTrace();
  }
 }
  public Xxys(){
  super();
  setTitle("小学四则运算系统");
  setBounds(0,0,400,500);
  getContentPane().setLayout(null);
  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

 //年级选择
  graselect=new JLabel();
  graselect.setBounds(0, 30, 50, 30);
  graselect.setText("年级：");
  getContentPane().add(graselect);
  //一年级
     buttonGroup=new ButtonGroup();
     gradeone =new JRadioButton();
     buttonGroup.add(gradeone);
     gradeone.setText("一年级");
     gradeone.setBounds(60, 30, 80, 30);
     gradeone.setSelected(true);
     getContentPane().add(gradeone);
  //二年级
     buttonGroup=new ButtonGroup();
     gradetwo =new JRadioButton();
     buttonGroup.add(gradetwo);
     gradetwo.setText("二年级");
     gradetwo.setBounds(140, 30, 80, 30);
     gradetwo.setSelected(true);
     getContentPane().add(gradetwo);
     //三年级
     gradethree =new JRadioButton();
     buttonGroup.add(gradethree);
     gradethree.setText("三年级");
     gradethree.setBounds(220, 30, 80, 30);
     //gradetwo.setSelected(false);
     getContentPane().add(gradethree);
     //四年级
     gradefour =new JRadioButton();
     buttonGroup.add(gradefour);
     gradefour.setText("四年级");
     gradefour.setBounds(300, 30, 80, 30);
     //gradetwo.setSelected(false);
     getContentPane().add(gradefour);
     //五年级
     gradefive =new JRadioButton();
     buttonGroup.add(gradefive);
     gradefive.setText("五年级");
     gradefive.setBounds(380, 30, 80, 30);
     //gradetwo.setSelected(false);
     getContentPane().add(gradefive);
      //六年级
     gradesix =new JRadioButton();
     buttonGroup.add(gradesix);
     gradesix.setText("六年级");
     gradesix.setBounds(460, 30, 80, 30);
     //gradetwo.setSelected(false);
     getContentPane().add(gradesix);
     

     questnum=new JLabel();
     questnum.setText("题目数量：");
     questnum.setBounds(0, 80, 80, 40);
     getContentPane().add(questnum);
  
     //题目数量
     textnum=new JTextField();
     textnum.setBounds(90,90, 80, 20);
     getContentPane().add(textnum);
     
      //出题的按钮
     chuti= new JButton();
     chuti.setText("出题");
     chuti.setBounds(60, 150, 60, 30);
    
     chuti.addActionListener(new chutiListener());
     getContentPane().add(chuti);
     
     //重置的按钮
     chongzhi =new JButton();
     chongzhi.setText("重置");
     chongzhi.setBounds(240, 150, 60, 30);
     chongzhi.addActionListener(new chongzhiListener());
     getContentPane().add(chongzhi);
    //出题的方框
      quest=new JTextArea();
      quest.setColumns(15);
      quest.setRows(3);
      quest.setLineWrap(true);
      JScrollPane scrollPane=new JScrollPane();
      scrollPane.setViewportView(quest);
      Dimension dime=quest.getPreferredSize();
      scrollPane.setBounds(0, 200, 400, 200);
      getContentPane().add(scrollPane);
     }
 //生成数的方法
 public  int shenchengnum(int a){
     Random rand=new Random();
     int k=0;
     int j=0;
     for(int i=0;i<a;i++)
     {
      j=rand.nextInt(9)+1;
      k=k*10+j;
     }
     return k;
    }
 public char shengchenchar(boolean zhfu){
     Random rand=new Random();
     char c=' ';
     if(zhfu){
      int j=rand.nextInt()%2;
       if(j==0)
       {
        c='-';
       }
      
     }
        return c;
     }
    public void fenshunum(int a,int b,int c){   
     a=shenchengnum(c);
     b=shenchengnum(c);
    }
    public String xiaoshusc(){
     Random rand=new Random();
     DecimalFormat dcmFmt = new DecimalFormat("0.00");
     float a=rand.nextFloat()*100;
        String b=dcmFmt.format(a);
        return b;    
        
    }
    public char yunsuanchar(boolean szyf){
     Random rand=new Random();
     int j=0;
     char c=' ';
     if(szyf){
      j=rand.nextInt(2);
       if(j==0){
        c='/';
       } else if(j==1){
           c='*';
          }
       }

else{
      j=rand.nextInt()%2;
      if(j==0){
       c='+';
      }
      else {
       c='-';
      }
     }
     return c;
    }
//生成数的方法
public void actionPerformed(ActionEvent e) {
  // TODO Auto-generated method stub
  String num=textnum.getText();
  int qtnum=Integer.parseInt(num);
   if(gradetwo.isSelected()){
    for(int i=0;i<qtnum;i++){
     Random rand=new Random();
     int k=rand.nextInt()%2;
     if(k==0){

        String fnum=String.valueOf(shenchengnum(4));
        String snum=String.valueOf(shenchengnum(4));
        String ch=String.valueOf(yunsuanchar(false));
              quest.append(fnum+" "+" "+ch+" "+snum+"=");
              quest.append("\n");
              }else{
              String fnum=String.valueOf(shenchengnum(2));
        String snum=String.valueOf(shenchengnum(1));
        String ch=String.valueOf(yunsuanchar(true));
              quest.append(fnum+" "+" "+ch+" "+snum+"=");
              quest.append("\n");
          }

     } 
    }
   else if(gradethree.isSelected()){
    for(int i=0;i<qtnum;i++){
     Random rand=new Random();
     int k=rand.nextInt()%2;
     if(k==0){
        String fnum=String.valueOf(shenchengnum(4));
        String snum=String.valueOf(shenchengnum(4));
        String ch=String.valueOf(yunsuanchar(false));
              quest.append(fnum+" "+" "+ch+" "+snum+"=");
              quest.append("\n");
              }else{
              String fnum=String.valueOf(shenchengnum(4));
        String snum=String.valueOf(shenchengnum(1));
        String ch=String.valueOf(yunsuanchar(true));
              quest.append(fnum+" "+" "+ch+" "+snum+"=");
              quest.append("\n");
          }
      }

}else if(gradefour.isSelected()){
    for(int i=0;i<qtnum;i++){
     String fnum=String.valueOf(shenchengnum(3));
        String snum=String.valueOf(shenchengnum(2));
        String ch=String.valueOf(yunsuanchar(true));
              quest.append(fnum+" "+" "+ch+" "+snum+"=");
              quest.append("\n"); 
    }
   }
   else if (gradefive.isSelected()){
    for(int i=0;i<qtnum;i++){
     String fnum=xiaoshusc();
        String snum=xiaoshusc();
        String ch=String.valueOf(yunsuanchar(true));
              quest.append(fnum+" "+" "+ch+" "+snum+"=");
              quest.append("\n");
  }
   }
}    
public class chutiListener implements ActionListener{

          @Override  public void actionPerformed(ActionEvent e) {             
    }    
   }
public class chongzhiListener implements ActionListener{

          @Override  public void actionPerformed(ActionEvent e) {

              textnum.setText(null);
                quest.setText(null);

    }    
   }

}
